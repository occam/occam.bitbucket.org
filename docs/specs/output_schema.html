<!DOCTYPE html>
<html>
  <head>
    <link href='/images/favicon-16.png' rel='icon' sizes='16x16'>
    <link href='/images/favicon-32.png' rel='icon' sizes='32x32'>
    <link href='/images/favicon-48.png' rel='icon' sizes='48x48'>
    <link href='/css/reset.css' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600|Cinzel+Decorative:700|Alegreya+Sans+SC|Marcellus+SC|inconsolata' rel='stylesheet' type='text/css'>
    <link href='/css/docs.css' rel='stylesheet' type='text/css'>
    <link href='/css/zenburn.css' rel='stylesheet' type='text/css'>
    <link href='/css/ui-lightness/jquery-ui-1.10.4.custom.min.css' rel='stylesheet' type='text/css'>
    
    <title>
      OCCAM System Documentation - Output Schema
    </title>
  </head>
  <body>
    <nav>
      <h1>
        Output Schema
      </h1>
      <ul>
        <li class='back'>
          <a href='/docs'>
            <span class='arrow'>
              &#x25c0;
            </span>
            Back
          </a>
        </li>
        <li class='top'>
          <a href='#'>
            <span class='arrow'>
              &#x25B2;
            </span>
            Top
          </a>
        </li>
        <li><a href='#type'>Type</a><ul><li><a href='#int'>int</a><ul></ul></li><li><a href='#float'>float</a><ul></ul></li><li><a href='#string'>string</a><ul></ul></li></ul></li><li><a href='#units'>Units</a><ul></ul></li><li><a href='#label'>Label</a><ul></ul></li>
      </ul>
    </nav>
    <div class='content'>
      <h1 class='big' id='top'>Output Schema</h1>
      <p>OCCAM provides a mechanism to generate graphs and understand results agnostic to the actual simulator used.
      To do this, each simulator describes the possible results it <em>may</em> generate as a <code>json</code> schema.
      Data is typically hierarchical.
      The schema simply represents each value where it would appear in this hierarchy.</p>
      
      <p>Each data point is described with its type (int, float, etc) and its units.
      Knowledge about the type of value will aid in the production of correct and relevant graphs and visualizations.</p>
      
      <p>Here is an example excerpt <code>output_schema.json</code> file for <a href='https://wiki.umd.edu/DRAMSim2' title=''>DRAMSim2</a> which you can also find in our <a href='https://bitbucket.org/occam/occam-dramsim2/src' title=''>code repository</a>:</p>
      
      <pre><code>{&#x000A;  &quot;data&quot;: {&#x000A;    &quot;channels&quot;: [&#x000A;      {&#x000A;        &quot;id&quot;: {&#x000A;          &quot;type&quot;: &quot;int&quot;,&#x000A;          &quot;label&quot;: true&#x000A;        },&#x000A;        &quot;bandwidth&quot;: {&#x000A;          &quot;type&quot;: &quot;float&quot;,&#x000A;          &quot;units&quot;: &quot;foo&quot;&#x000A;        },&#x000A;        &quot;ranks&quot;: [&#x000A;          {&#x000A;            &quot;writes&quot;: {&#x000A;              &quot;type&quot;: &quot;int&quot;&#x000A;            },&#x000A;            &quot;writesSize&quot;: {&#x000A;              &quot;type&quot;: &quot;int&quot;,&#x000A;              &quot;units&quot;: &quot;bytes&quot;&#x000A;            },&#x000A;            &quot;reads&quot;: {&#x000A;              &quot;type&quot;: &quot;int&quot;&#x000A;            }&#x000A;            &quot;readsSize&quot;: {&#x000A;              &quot;type&quot;: &quot;int&quot;,&#x000A;              &quot;units&quot;: &quot;bytes&quot;&#x000A;            },&#x000A;            &quot;latency&quot;: [&#x000A;              {&#x000A;                &quot;max&quot;: &quot;int&quot;,&#x000A;                &quot;amount&quot;: &quot;int&quot;,&#x000A;                &quot;min&quot;: &quot;int&quot;&#x000A;              }&#x000A;            ],&#x000A;          }&#x000A;        ]&#x000A;      }&#x000A;    ]&#x000A;  }&#x000A;}&#x000A;</code></pre>
      
      <p>In this example, the simulator will output its results as a dictionary object. For DRAMSim2, there is a script that will run after the simulation is complete that will parse the specific output so that it corresponds to this schema. This script is unique to every simulator and must be written if the simulator does not natively output JSON to match its provided OCCAM schema.</p>
      
      <p>The schema is described within a key <code>data</code>.</p>
      
      <p>The key <code>channels</code> is an array of data.
      Each channel element contains an <code>id</code>, a float representing <code>bandwidth</code>, and an array of <code>ranks</code>.
      Ranks in turn each contain a number of <code>writes</code> and the number of <code>bytes</code> these writes represent.
      Each rank contains an array of <code>latency</code> tuples consisting of a <code>max</code>, <code>amount</code>, and <code>min</code> which are all integers.</p>
      
      <p>When an array is expected, you use an array object in the json.
      It will always have only one item in it: the description of what type of element to expect in the array.
      Arrays, as such, can only represent a single data-type.</p>
      
      <p>A Hash (key/value) object is specified as a json dictionary object.
      In the above, <code>channels</code> is an array of hash objects containing values for <code>id</code>, <code>bandwidth</code> and <code>ranks</code> keys.</p>
      
      <p>Actual data-types are the leaves of this hierarchical representation.
      Essentially, an object with a <code>type</code> key with a string value describes a data-type and not a Hash object.</p>
      
      <p>Data-types can be described as simply a name and then a type:</p>
      
      <pre><code>&quot;reads&quot;: {&#x000A;  &quot;type&quot;: &quot;int&quot;&#x000A;}&#x000A;</code></pre>
      
      <p>This creates a data-type called &#39;reads&#39; expected a value of an integer. More complicated data-types can be created from optional values described later on in this document:</p>
      
      <pre><code>&quot;writesSize&quot;: {&#x000A;  &quot;type&quot;: &quot;int&quot;,&#x000A;  &quot;units&quot;: &quot;bytes&quot;&#x000A;}&#x000A;</code></pre>
      
      <p>Here, <code>writesSize</code> is an integer that represents a number of bytes.
      Unit information is critical for ensuring accurate visualizations and avoiding human error.</p>
      
      <pre><code>&quot;latency&quot;: [&#x000A;  {&#x000A;    &quot;max&quot;: {&#x000A;      &quot;type&quot;: &quot;int&quot;&#x000A;    },&#x000A;    &quot;amount&quot;: {&#x000A;      &quot;type&quot;: &quot;int&quot;,&#x000A;      &quot;units&quot;: &quot;nanoseconds&quot;&#x000A;    },&#x000A;    &quot;min&quot;: {&#x000A;      &quot;type&quot;: &quot;int&quot;&#x000A;    }&#x000A;  }&#x000A;]&#x000A;</code></pre>
      
      <p>In this example, <code>latency</code> is an array of objects with three integer fields.
      The <code>amount</code> field has units in nanoseconds.
      Descriptions of each parameter used to define data-types are listed below.</p>
      <h2 id='type'>Type</h2><h3 id='int'>int</h3><h3 id='float'>float</h3><h3 id='string'>string</h3><h2 id='units'>Units</h2>
      <p>A free-form string that describes the unit (as a plural) for this data.</p>
      
      <pre><code>&quot;averagePower&quot;: {&#x000A;  &quot;type&quot;: &quot;float&quot;,&#x000A;  &quot;units&quot;: &quot;watts&quot;&#x000A;}&#x000A;</code></pre>
      
      <p>The above describes <code>averagePower</code> as being a floating point value determining the number of <code>watts</code> in average is being consumed.
      When graphs are produced from this value, the default label for the axis representing the data will describe the units.
      The presence of this field will make it less likely for human error to misrepresent data.</p>
      <h2 id='label'>Label</h2>
      <p>A boolean value.
      When <code>true</code>, then this data type is typically not used as a data-point, but rather as graph labels.</p>
    </div>
    <script src='/js/jquery/jquery.min.js'></script>
    <script src='/js/jquery.nav.js'></script>
    <script src='/js/docs.js'></script>
    
  </body>
</html>
